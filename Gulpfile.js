'use strict';

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    traceur = require('gulp-traceur'),
    jshint = require('gulp-jshint'),
    coffeelint = require('gulp-coffeelint'),
    jade = require('gulp-jade'),
    coffee = require('gulp-coffee'),
    mocha = require('gulp-mocha'),
    nodemon = require('gulp-nodemon'),
    karma = require('gulp-karma');

gulp.task('mocha', function () {
    gulp.src(['build/*.js','spec/server/*.coffee'])
        .pipe(mocha({
            reporter: 'nyan'
        }));
});

gulp.task('karma', ['client-es6', 'client-coffee'], function() {
    return gulp.src([
        'build/public/js/*.js',
        'spec/client/**/*.coffee'
    ]).pipe(karma({
        configFile: 'karma.conf.js',
        action: 'run'
    })).on('error', function(err) {
        throw err;
    });
});

gulp.task('jshint', function() {
    return gulp.src('src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});
gulp.task('coffee-lint', function() {
    return gulp.src('./src/**/*.coffee')
        .pipe(coffeelint())
        .pipe(coffeelint.reporter());
});

var traceurOptions = {
    sourceMap: true,
    generators: 'parse',
    arrowFunctions: true,
    defaultParameters : true,
    destructuring : true,
    restParameters : true,
    spread : false,
    arrayComprehension: false,
    classes : false,
    computedPropertyNames : false,
    forOf : false,
    generatorComprehension : false,
    modules : false,
    numericLiterals : false,
    propertyMethods : false,
    propertyNameShorthand : false,
    templateLiterals : false
};

gulp.task('server-es6', function () {
    return gulp.src('src/server/js/*.js')
        .pipe(traceur(traceurOptions))
        .pipe(gulp.dest('build'));
});

gulp.task('client-es6', function () {
    return gulp.src('src/client/js/*.js')
        .pipe(traceur(traceurOptions))
        .pipe(gulp.dest('build/public/js'));
});

gulp.task('jade', function() {
    return gulp.src('src/*.jade')
        .pipe(jade({ pretty : true }))
        .pipe(gulp.dest('build/public/html'));
});

gulp.task('server-coffee', function() {
    return gulp.src('src/server/coffee/**/*.coffee')
        .pipe(coffee({ bare: true }).on('error', gutil.log))
        .pipe(gulp.dest('build'));
});

gulp.task('client-coffee', function() {
    return gulp.src('src/client/coffee/**/*.coffee')
        .pipe(coffee({ bare: true }).on('error', gutil.log))
        .pipe(gulp.dest('build/public/js'));
});

gulp.task('lint', ['jshint', 'coffee-lint']);
gulp.task('es6', ['server-es6', 'client-es6']);
gulp.task('coffee', ['server-coffee', 'client-coffee']);
gulp.task('spec', ['mocha', 'karma']);

gulp.task('watch', function() {
    gulp.watch('src/client/coffee/*.coffee', ['client-coffee']);
    gulp.watch('src/server/coffee/*.coffee', ['server-coffee']);
    gulp.watch('src/server/js/*.js', ['server-es6']);
    gulp.watch('src/client/js/*.js', ['client-es6']);
    gulp.watch('src/client/jade/*.jade', ['jade']);
//    gulp.watch(['spec/server/*.coffee', 'build/*.js'], ['mocha']);
    gulp.watch(['spec/client/*.coffee', 'build/public/*.js'], ['karma']);

});

gulp.task('nodemon', function() {
    nodemon({
        script: 'build/app.js',
        ext: 'html js',
        watch: ['./build/**'],
        ignore: ['./build/js/**', './build/html/*'],
        env: {'NODE_ENV' : 'development'},
        nodeArgs: ['--debug', '--harmony']
    });
});

gulp.task('default', [
    'lint',
    'server-es6',
    'client-es6',
    'server-coffee',
    'client-coffee',
    'jade',
    'karma',
    'watch',
    'nodemon'
]);
