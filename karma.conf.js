module.exports = function(config) {
    config.set({
        // base path for all patterns (eg. files, exclude)
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'build/public/**/*.js',
            'spec/**/*.coffee'
        ],
        exclude: [

        ],
        preprocessors: {
            'spec/**/*.coffee': ['coffee']
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
};
