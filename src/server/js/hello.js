'use strict';

var map = function*(g, f) {
    for(var e = g.next(); !e.done; e = g.next())
        yield f(e);
};

var filter = function*(g, f) {
    for(var e = g.next(); !e.done; e = g.next())
        if(f(e))
            yield e;
};

var indexOf = function(g, f) {
    var index = 0;
    for(var e = g.next(); !e.done; e = g.next(), ++index)
        if(f(e))
            return index;
    return -1;
};


var map = function*(g, f) {
    for(var e = g.next(); !e.done; e = g.next())
        yield f(e);
};
